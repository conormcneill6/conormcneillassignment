// AccountServer.java
package Server;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

import org.omg.CORBA.ORB;

import AccountEx.Account;

public class AccountServer {
  public static void main(String[] args) {
     try {
       // create and initialize the ORB
       ORB orb = ORB.init(args, null);
       // create imlementation object 
       Account account = new  AccountImplementation("peter"); 
       // convert to stringified object reference
       String sor = orb.object_to_string(account); 
       // write stringified object reference to file
       ObjectOutputStream out = new ObjectOutputStream(
   				new FileOutputStream("IOR")) ;
       out.writeObject(sor) ;
       out.close() ;
       orb.run();
     } catch (Exception e) {}
   }
}
