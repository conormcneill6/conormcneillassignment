package Server;

//AccountImplementation.java

import AccountEx._AccountImplBase;

public class AccountImplementation extends _AccountImplBase {
	public String name;
	public float bal;

	public AccountImplementation(String name) {
		this.name = name;
		this.bal = 0;
	}

	public String owner() {
		return name;
	}

	public float balance() {
		return bal;
	}

	public void deposit(float amount) {
		bal = bal + amount;
	}

	public void withdraw(float amount) {
		bal = bal - amount;
	}
}
