package Client;

//AccountClient.java

import AccountEx.*;
import org.omg.CORBA.*;
import java.io.*;

public class AccountClient {
	public static void main(String[] args) {
		try {
			// create and initialize the ORB
			ORB orb = ORB.init(args, null);
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(
					"IOR"));
			String sor = (String) in.readObject();
			in.close();

			org.omg.CORBA.Object obj = orb.string_to_object(sor);
			Account account = AccountHelper.narrow(obj);
			System.out.println("Account holder is " + account.owner());

			System.out.println("Depositing 100 pounds  ");
			account.deposit(100);
			System.out.println("Balance is now " + account.balance());

			System.out.println("Withdrawing 50 pounds  ");
			account.withdraw(50);
			System.out.println("Balance is now " + account.balance());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
