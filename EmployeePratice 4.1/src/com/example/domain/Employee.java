package com.example.domain;

import java.text.NumberFormat;

public class Employee {
	
	private int empid;
	private String empName;
	private String empSocSec;
	private double empSal;
	
	public Employee(int empid, String empName, String empSocSec, double empSal){
		this.empid = empid;
		this.empName = empName;
		this.empSocSec = empSocSec;
		this.empSal = empSal;
	}
	
	
	@Override
	public String toString() {
		return "Employee [empid=" + empid + "\n" + 
				", empName=" + empName +"\n" + ", " +
				"empSocSec=" + empSocSec + "\n" + ", empSal=" 
				+ NumberFormat.getCurrencyInstance().format(getEmpSal())+ "\n" + "]";
	}


	public void raiseSalary (double increase){
		empSal += increase;
	}

	public int getEmpid() {
		return empid;
	}

//	public void setEmpid(int empid) {
//		this.empid = empid;
//	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpSocSec() {
		return empSocSec;
	}

//	public void setEmpSocSec(String empSocSec) {
//		this.empSocSec = empSocSec;
//	}

	public double getEmpSal() {
		return empSal;
	}

//	public void setEmpSal(double empSal) {
//		this.empSal = empSal;
//	}
}
