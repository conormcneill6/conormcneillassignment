package com.example.domain;

public class Director extends Manager{

	private double budget;
	public Director(int empid, String empName, String empSocSec, double empSal,
			String deptName, double budget) {
		super(empid, empName, empSocSec, empSal, deptName);
		this.budget=budget;
	}
	public double getBudget() {
		return budget;
	}
	public void setBudget(double budget) {
		this.budget = budget;
	}
	@Override
	public String toString() {
		return super.toString() + "Director [budget=" + budget + "]";
	}
	
	
}
