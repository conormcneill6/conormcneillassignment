package com.example.domain;

import java.util.Arrays;

public class Manager extends Employee{

	private Employee[] staff;
	private int employeeCount =0;
	private String deptName;
	
	public Manager(int empid, String empName, String empSocSec, double empSal, String deptName ) {
		super(empid, empName, empSocSec, empSal);
		this.deptName = deptName;
		this.staff = new Employee[20];

	}
	
	
	@Override
	public String toString() {
		return super.toString()+ "\nDepartment: " +" deptName=" + deptName + "]";
	}



	public int findEmployee(Employee e){
		int result = -1;
		for(int i = 0; i < staff.length; i++){
			if (e.equals(staff[i])){
				result = i;
			}
		}	
		return result;
	}
	public boolean addEmployee(Employee e){
		if(findEmployee(e) != -1) return false;
		
		staff[employeeCount]=e;
		employeeCount++;
		return true;
	}
	
	public boolean removeEmployee(Employee e){
		boolean empPartOfStaff = false;
		Employee [] newStaff = new Employee[20];
		int newEmpCount =0;
		for(int i =0; i < employeeCount; i++){
		
			if(staff[i].getEmpid() == e.getEmpid()){
				empPartOfStaff= true;
			}else{
				newStaff[newEmpCount]=staff[i];
				newEmpCount++;
			}
		}
		if(empPartOfStaff){
			staff = newStaff;
			employeeCount = newEmpCount;
		}
		return empPartOfStaff;
	}
	
	public void printStaffDetails(){
		System.out.println("Staff of "+this.getEmpName());
		for(int i=0; i<employeeCount; i++){
			System.out.println("Name "+ staff[i].getEmpName() + staff[i].getEmpid());
			
		}
	}
	
	public String getDeptName() {
		return deptName;
	}
}
