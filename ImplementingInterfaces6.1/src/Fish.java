
public class Fish extends Animal implements Pet {

	private String name;
	protected Fish() {
		super(0);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		name = name;
		
	}

	@Override
	public void play() {
		System.out.println("Fish dont play");
		
	}

	@Override
	public void eat() {
		System.out.println("Fish eat fish food");
		
	}
	
	@Override 
	public void walk(){
		super.walk();
		System.out.println("Fish dont walk");
	}

	
}
