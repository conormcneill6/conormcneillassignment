package com.example.dao;

import java.util.ArrayList;
import java.util.List;

import com.example.model.Employee;

public class EmployeeDAOMemoryImpl implements EmployeeDAO {

	private static Employee[] employeeArray = new Employee[10];

	public EmployeeDAOMemoryImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void delete(final int id) {
		employeeArray[id] = null;
	}

	// Find an Employee record using this ID
	@Override
	public Employee findById(final int id) {
		return employeeArray[id];
	}

	// Return an array of all of the Employee records
	// We are using a collection List object to store the results
	// This makes it easier to just add to the collection
	@Override
	public Employee[] getAllEmployees() {
		final List<Employee> emps = new ArrayList<>();
		// Iterate through the memory array and find Employee objects
		for (final Employee e : employeeArray) {
			if (e != null) {
				emps.add(e);
			}
		}
		return emps.toArray(new Employee[0]);
	}

	@Override
	public void add(final Employee emp) {
		employeeArray[emp.getId()] = emp;

	}

	@Override
	public void update(final Employee emp) {
		employeeArray[emp.getId()] = emp;

	}

}
