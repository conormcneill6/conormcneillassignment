package com.ait.example.Nesting;

public class OuterClass {

	private int x=42;
	private static int z = 45;
	
	public void method1(){//OuterClass$1LocalClass.class
		class LocalClass{
			public void localPrint(){
				System.out.println("in local class");
				System.out.println("x == "+x);
			}
		}
		
		LocalClass lc = new LocalClass();
		lc.localPrint();
	}
	
	public void method2(){//anonymous class OuterClass$1.class 
		Runnable r = new Runnable(){
			@Override
			public void run(){
				System.out.println("in anonymous local method");
				System.out.println("x == "+x);
			}
		};
		r.run();
	}
	
	public Runnable r = new Runnable(){//OuterClass$2.class
		@Override
		public void run(){
			System.out.println("in anonymous local method");
			System.out.println("x == "+x);
		}
	};
	Object o = new Object(){//this is a lining a class that extends object OuterClass$3.class
		@Override
		public String toString(){
			return "in an anonymous class method";
		}
	};
	public class innerClass{
		//this x here hides the outer class x
		private int x = 43;
		
		public void innerPrint(){
			System.out.println("in inner class method:: innerPrint()");
			System.out.println("x == "+x);
		}
	}
	
	public static class StaticNestedClass{
		public void staticNextedPrint(){
			System.out.println("in a static nested method:: staticNextedPrint()");
			System.out.println("z == "+z);
		}
	}
	public class A{//$A
		public class B{//$B
			public void method(){
				class C{//$AB$1C.class
					
				}
			}
		}
	}
	
}
