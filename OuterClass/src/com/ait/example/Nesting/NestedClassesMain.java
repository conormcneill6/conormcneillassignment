package com.ait.example.Nesting;

public class NestedClassesMain {

	public static void main(String[] args) {
		OuterClass oc = new OuterClass();
		
		System.out.println("oc.method1()");
		oc.method1();
		System.out.println();
		
		System.out.println("oc.method2()");
		oc.method2();
		System.out.println();
		
		System.out.println("oc.r.run()");
		oc.r.run();
		System.out.println();
		
		OuterClass.innerClass ic = oc.new innerClass();
		System.out.println("ic.innerPrint()");
		ic.innerPrint();
		System.out.println();
		
		OuterClass.StaticNestedClass sn = new OuterClass.StaticNestedClass();
		System.out.println("ic.staticNestedPrint()");
		sn.staticNextedPrint();
		System.out.println();
		
		OuterClass.A.B nested = oc.new A().new B();
	}

}
