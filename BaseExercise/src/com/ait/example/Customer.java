package com.ait.example;

public class Customer {

	private String firstName;
	private String lastName;
	private int numberOfAccounts;
	public Account [] Account;
	
	public Customer(String f, String l){
		firstName = f;
		lastName = l;
		 // initialize accounts array
        Account = new Account[10];
        numberOfAccounts = 0;
		
	}
	
	public String getFirstName(){
		return firstName;
	}
	
	public String getLastName(){
		return lastName;
	}
	
	public void addAccount(Account account){
		int i = numberOfAccounts++;
		Account[i] = account;
	}
	
	public int getNumberOfAccounts(){
		return numberOfAccounts;
	}
	
	public Account getAccount(int i){
		return Account[i];
	}
}
