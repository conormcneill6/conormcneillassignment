package com.example.domain;

public class Employee {
	
	public int empid;
	public String empName;
	public String empSocSec;
	public double empSal;
	
	public void Employee(){
		this.empid = empid;
		this.empName = empName;
		this.empSocSec = empSocSec;
		this.empSal = empSal;
	}

	public int getEmpid() {
		return empid;
	}

	public void setEmpid(int empid) {
		this.empid = empid;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpSocSec() {
		return empSocSec;
	}

	public void setEmpSocSec(String empSocSec) {
		this.empSocSec = empSocSec;
	}

	public double getEmpSal() {
		return empSal;
	}

	public void setEmpSal(double empSal) {
		this.empSal = empSal;
	}
}
