package com.example;

import com.example.domain.Employee;

public class EmployeeTest {

	public static void main(String[] args) {
		Employee e = new Employee();
		e.setEmpid(101);
		e.setEmpName("Jane Smith");
		e.setEmpSocSec("012-34-4567");
		e.setEmpSal(120_345.27);
		System.out.println("Emp id "+e.getEmpid());
		System.out.println("Emp Name "+e.getEmpName());
		System.out.println("Emp Social Security Number "+e.getEmpSocSec());
		System.out.println("Emp Salsry "+ e.getEmpSal());

	}

}
