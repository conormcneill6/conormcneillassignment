package collections;

import java.util.HashSet;
import java.util.Set;

public class RemoveDuplicates {

	public static void main(final String[] args) {
		String toungTwister = "I feel, a feel, a funny feel, ";
		toungTwister += "a funny feel I feel, if you feel the feel ";
		toungTwister += "I feel, I feel the feel you feel";

		final Set<String> words = new HashSet<>();

		for (final String word : toungTwister.split("\\W+")) {
			words.add(word);
		}

		System.out.println("The words that were used ");
		System.out.println(words);
	}
}
