package genericsSlide29;

import java.util.ArrayList;
import java.util.List;

public class GenericTest {
	public static void main(final String[] args) {
		final List<Number> al = new ArrayList<Number>();
		final List<Number> al2 = new ArrayList<>();

		final List<Integer> arrayListOfInteger = new ArrayList<Integer>();
		arrayListOfInteger.add(4);
		arrayListOfInteger.add(4);
		arrayListOfInteger.add(3);
		arrayListOfInteger.add(0, 5);
		arrayListOfInteger.add(2, 6);
		System.out.println(arrayListOfInteger);

		final List<Number> arrayListOfNumber = new ArrayList<Number>(
				arrayListOfInteger);
		System.out.println(arrayListOfNumber);
	}
}
