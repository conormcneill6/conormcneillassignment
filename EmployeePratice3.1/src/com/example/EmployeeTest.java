package com.example;

import com.example.domain.Admin;
import com.example.domain.Director;
import com.example.domain.Employee;
import com.example.domain.Engineer;
import com.example.domain.Manager;

public class EmployeeTest {

	public static void main(String[] args) {
		Engineer eng = new Engineer(101, "Jane Smith", "012-34-5678", 120_345.27);
		Manager man = new Manager(201, "Barbra Johnson", "054-12-2367", 109_501.36, "US Marketing");
		Admin adm = new Admin(304, "Bill Monroe", "108-23-6509", 75_002.34);
		Director dir = new Director(12, "Susan Wheeler", "099-45-2340", 120_567.36, "Global Marketing", 1_000_000.00);
		printEmployee(eng);
		printEmployee(man);
		printEmployee(adm);
		printEmployee(dir);
		
		

	}
	
	public static void printEmployee(Employee emp){
		
		System.out.println("Emp id "+emp.getEmpid());
		System.out.println("Emp Name "+emp.getEmpName());
		System.out.println("Emp Social Security Number "+emp.getEmpSocSec());
		System.out.println("Emp Salsry "+ emp.getEmpSal());
		System.out.println();
	}

}
