package com.ait.example;

import java.util.Calendar;
import static com.ait.example.DepositLength.*;
public class EnumBankingMain {

	public static void main(String[] args) {
		Bank b = new Bank();
		initializeCustomer(b);
		CustomerReport cr = new CustomerReport();
		cr.setBank(b);
		cr.generateReport();
	}
	
	private static void initializeCustomer(Bank bank){
		Customer customer;
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR, 180);
		
		bank.addCustomer("Jane", "Simms");
		customer = bank.getCustomer(0);
		customer.addAccount(new TimeDepositAccount(500.00, SIX_MONTHS));
		customer.addAccount(new CheckingAccount(200.00, 400.00));
		
		bank.addCustomer("Owen", "Bryant");
		customer = bank.getCustomer(1);
		customer.addAccount(new CheckingAccount(200.00));
		
		bank.addCustomer("Tim", "Soley");
		customer = bank.getCustomer(2);
		customer.addAccount(new TimeDepositAccount(1500.00, THREE_MONTHS));
		customer.addAccount(new CheckingAccount(200.00));
		
		bank.addCustomer("Maria", "Soley");
		customer = bank.getCustomer(3);
		customer.addAccount(bank.getCustomer(2).getAccount(1));
		customer.addAccount(new TimeDepositAccount(150.00, SIX_MONTHS));
		
	}
}
