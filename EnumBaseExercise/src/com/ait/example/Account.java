package com.ait.example;

public abstract class Account {
	protected double balance;
	
	public Account(double balance){
		this.balance = balance;
	}
	public double getBalance(){
		return balance;
	}
	
	@Override
	public String toString(){
		return getDescription() + ": current balance is " + balance;
	}
	
	public void deposit(double amount){
		balance += amount;
	}
	
	public abstract String getDescription();
	
	public abstract boolean withdraw(double amount);
	
}
