package com.ait.example;

public class Bank {
	private Customer [] customer;
	private int numberOfCustomers;
	
	public  Bank(){
		customer = new Customer[10];
		numberOfCustomers = 0;
		
	}
	
	public void addCustomer(String f, String l){
		int i = numberOfCustomers ++;
		customer[i] = new Customer(f,l);
	}
	
	public int getNumberOfCustomers(){
		return numberOfCustomers;
	}
	
	public Customer getCustomer(int cust){
		return customer[cust];
	}
}
