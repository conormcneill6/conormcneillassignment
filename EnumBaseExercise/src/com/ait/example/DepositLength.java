package com.ait.example;

public enum DepositLength {
	
	THREE_MONTHS(90), SIX_MONTHS(180);
	
	private int days;
	
	private DepositLength(int day){
		this.days=days;
	}
	
	public int getDays(){
		return days;
	}
}
