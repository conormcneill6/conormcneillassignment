package com.ait.example;

public class CustomerReport {
	private Bank bank;
	
	public Bank getBank(){
		return bank;
	}
	
	public void setBank(Bank bank){
		this.bank = bank;
	}
	
	public void generateReport(){
		 System.out.println("\t\t\tCUSTOMERS REPORT");
	        System.out.println("\t\t\t================");
		for(int i = 0; i < bank.getNumberOfCustomers(); i++){
			Customer customer = bank.getCustomer(i);
			System.out.println();
			System.out.println("Customer "+customer.getLastName()+" , "+ customer.getFirstName());
			for(int j =0; j < customer.getNumberOfAccounts(); j++){
				Account account = customer.getAccount(j);

                // Print the current balance of the account
                System.out.println("    " + account);
			}
		}
	}
}
