package com.ait.example;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeDepositAccount extends Account {
	
	
	private Date maturityDate;
	
	public TimeDepositAccount(double balance, DepositLength duration ){
		 super(balance);
		 Calendar cal = Calendar.getInstance();
		 cal.add(Calendar.DAY_OF_YEAR, duration.getDays());
		 this.maturityDate = cal.getTime();
	}
	
	
	public boolean withdraw(double amount){
		Date date = new Date();
		if(date.after(maturityDate)){
			if(balance >= amount){
				balance -= amount;
				return true;
			}else return false;
		}else return false;
	}


	@Override
	public String getDescription() {
		return "Time Deposit Account " + maturityDate;
	}
}
